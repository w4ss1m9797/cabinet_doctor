package com.cd.cabinet.docteur.repository;


import com.cd.cabinet.docteur.domain.Agenda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgendaRepository extends JpaRepository<Agenda,Integer> {
}
