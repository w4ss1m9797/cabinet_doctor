package com.cd.cabinet.docteur.repository;

import com.cd.cabinet.docteur.domain.Agenda;
import com.cd.cabinet.docteur.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient,Integer> {


    Optional<Patient> findByLoginAndPwd(String login , String pwd);
}
