package com.cd.cabinet.docteur.repository;

import com.cd.cabinet.docteur.domain.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultation,Integer> {
}
