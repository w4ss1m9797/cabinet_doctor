package com.cd.cabinet.docteur.repository;

import com.cd.cabinet.docteur.domain.Agenda;
import com.cd.cabinet.docteur.domain.MedCertif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedCertifRepository extends JpaRepository<MedCertif,Integer> {
}
