package com.cd.cabinet.docteur.repository;

import com.cd.cabinet.docteur.domain.DossierMed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DossierMedRepository extends JpaRepository<DossierMed,Integer> {
}
