package com.cd.cabinet.docteur.ressource;


import com.cd.cabinet.docteur.domain.Agenda;
import com.cd.cabinet.docteur.service.AgendaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AgendaRessource {


    private final AgendaService agendaService;

    @GetMapping("/Agenda")
    public ResponseEntity<Agenda> getAgenda() {
        return ResponseEntity.ok().body(agendaService.getAgenda());
    }


}
