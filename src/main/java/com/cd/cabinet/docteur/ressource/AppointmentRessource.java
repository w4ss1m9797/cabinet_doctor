package com.cd.cabinet.docteur.ressource;


import com.cd.cabinet.docteur.domain.Agenda;
import com.cd.cabinet.docteur.domain.Appointment;
import com.cd.cabinet.docteur.domain.Patient;
import com.cd.cabinet.docteur.service.AgendaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AppointmentRessource {


    private final AgendaService agendaService;

    @PostMapping("/Agenda/{codeAgenda}/{codePatient}/addAppointement")
    public ResponseEntity<Agenda> saveAppointement(@RequestBody Appointment appointment, @PathVariable Integer codeAgenda, @PathVariable Integer codePatient) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/agenda/"+codeAgenda).toUriString());
        return ResponseEntity.created(uri).body(agendaService.updateAgenda(codeAgenda,appointment,codePatient));
    }
}
