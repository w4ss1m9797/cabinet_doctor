package com.cd.cabinet.docteur.ressource;


import com.cd.cabinet.docteur.Utilities.UserForm;
import com.cd.cabinet.docteur.domain.MedCertif;
import com.cd.cabinet.docteur.domain.Patient;
import com.cd.cabinet.docteur.service.PatientService;
import com.lowagie.text.DocumentException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PatientRessource {

    private final PatientService patientService;


    @PostMapping("/patients/register")
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/patients/register").toUriString());
        return ResponseEntity.created(uri).body(patientService.savePatient(patient));
    }


    @PostMapping("/patients/login")
    public ResponseEntity<Patient> checkLogin(@RequestBody UserForm userForm) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/patients/login").toUriString());
        Optional<Patient> patient = patientService.checkLogin(userForm);
        return ResponseEntity.created(uri).body(patient.isPresent() ?  patient.get() :  null);
    }


    @PostMapping("/patients/requestMedCertif/{codePatient}")
    public ResponseEntity<InputStreamResource> requestMedCertif(@RequestBody MedCertif medCertif, @PathVariable Integer codePatient) throws DocumentException, IOException {
        File document = patientService.requestMedCertif(medCertif,codePatient);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(document));
        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + document.getName())
                // Content-Type
                .contentType(MediaType.parseMediaType("application/pdf"))
                // Contet-Length
                .contentLength(document.length()) //
                .body(resource);
    }







}
