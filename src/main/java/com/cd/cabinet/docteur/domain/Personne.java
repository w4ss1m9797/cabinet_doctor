package com.cd.cabinet.docteur.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.text.DateFormat;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@MappedSuperclass
public class Personne {

    //@Id(GenerationType = GenerationType.AUTO)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer code;

    private Integer cin;

    private String photo;

    private String firstName;

    private String lastName;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dateNaiss;

    private String login;

    private String pwd;

    private String mail;

    private Integer telephone;







}
