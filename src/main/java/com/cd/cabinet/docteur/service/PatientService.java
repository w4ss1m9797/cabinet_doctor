package com.cd.cabinet.docteur.service;


import com.cd.cabinet.docteur.Utilities.UserForm;
import com.cd.cabinet.docteur.domain.MedCertif;
import com.cd.cabinet.docteur.domain.Patient;
import com.cd.cabinet.docteur.repository.MedCertifRepository;
import com.cd.cabinet.docteur.repository.PatientRepository;
import com.lowagie.text.Font;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import javax.transaction.Transactional;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PatientService {


    private final PatientRepository patientRepository;
    private final MedCertifRepository medCertifRepository;

    public Patient savePatient(Patient patient) {
        log.info("Saving new Patient {} to db", patient.getFirstName());
        return patientRepository.save(patient);
    }


    public Optional<Patient> checkLogin(UserForm userForm) {
        log.info("checkLogin {}", userForm.getLogin());
        Optional<Patient> patient = patientRepository.findByLoginAndPwd(userForm.getLogin(),userForm.getPwd());
        return patient;
    }


    public File requestMedCertif( MedCertif medCertif ,Integer codePatient) throws IOException {
        log.info("Retrieving Agenda from db");
        Patient patient =  patientRepository.getById(codePatient);
        medCertif.setPatient(patient);
        MedCertif medCertiff = medCertifRepository.save(medCertif);

        return export(medCertif);

    }

    public File export(MedCertif medCertif) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        File file = new ClassPathResource("static/pdf.pdf").getFile();
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);
        Paragraph p = new Paragraph("certif medical ..", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setSize(12);
        Paragraph p1 = new Paragraph("Je soussigné(e) . Docteur en médecine : wassim souilah .. atteste avoir examiné Monsieur (Madame) " +
                medCertif.getPatient().getFirstName() + "bla bla", font);
        document.add(p1);

        document.close();
        return file;
    }



}
