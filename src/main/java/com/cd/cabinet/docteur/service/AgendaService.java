package com.cd.cabinet.docteur.service;


import com.cd.cabinet.docteur.domain.Agenda;
import com.cd.cabinet.docteur.domain.Appointment;
import com.cd.cabinet.docteur.domain.Patient;
import com.cd.cabinet.docteur.repository.AgendaRepository;
import com.cd.cabinet.docteur.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RequiredArgsConstructor
@Transactional
@Slf4j
@Service
public class AgendaService {


    @Autowired
    private JavaMailSender mailSender;
    private final AgendaRepository agendaRepository;
    private final PatientRepository patientRepository;



    public Agenda getAgenda() {
        log.info("Retrieving Agenda from db");
        return agendaRepository.findAll().get(0);
    }

    public Agenda updateAgenda(Integer codeAgenda, Appointment appointment, Integer codePatient) {
        log.info("Retrieving Agenda from db");
        Agenda agenda =  agendaRepository.getById(codeAgenda);
        Patient patient = patientRepository.getById(codePatient);
        appointment.setPatient(patient);
        agenda.getAppointments().add(appointment);
        Agenda result = agendaRepository.save(agenda);
        log.info("sending mail .. !");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        sendMail(patient.getMail(),"Schedule Appointment CITA","Your appointment is scheduled for "+ LocalDateTime.parse(appointment.getTime().toString(), formatter)+", we have already sent you mail check it for more details");
        log.info("updating Agenda !");

        return  result;
    }
    /**
     * This method will send compose and send the message
     * */
    public void sendMail(String to, String subject, String body)
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

    public void sendMailWithAttachment(String to, String subject, String body, String fileToAttach)
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator()
        {
            public void prepare(MimeMessage mimeMessage) throws Exception
            {
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(new InternetAddress("wsouilah97@gmail.com"));
                mimeMessage.setSubject(subject);
                mimeMessage.setText(body);

                FileSystemResource file = new FileSystemResource(new File(fileToAttach));
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
                helper.addAttachment("logo.jpg", file);
            }
        };

        try {
            mailSender.send(preparator);
        }
        catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }

    public void sendMailWithInlineResources(String to, String subject, String fileToAttach)
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator()
        {
            public void prepare(MimeMessage mimeMessage) throws Exception
            {
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(new InternetAddress("admin@gmail.com"));
                mimeMessage.setSubject(subject);

                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

                helper.setText("<html><body><img src='cid:identifier1234'></body></html>", true);

                FileSystemResource res = new FileSystemResource(new File(fileToAttach));
                helper.addInline("identifier1234", res);
            }
        };

        try {
            mailSender.send(preparator);
        }
        catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }

}
