package com.cd.cabinet.docteur.config;


import com.google.common.base.Predicate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;
@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
                .apiInfo(apiInfo()).select().build();
    }

    private Predicate<String> postPaths() {
        return or(regex("/api/patients.*"), regex("/api/Agenda.*"), regex("/api/Agenda.*"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("cabMed API")
                .description("cabMed API reference for developers")
                .termsOfServiceUrl("http://w4ss1m.com")
                .contact("w4ss1m@gmail.com").license("CabMed License")
                .licenseUrl("w4ss1m@gmail.com").version("1.0").build();
    }

}
