package com.cd.cabinet.docteur.Utilities;

import lombok.Data;

@Data
public class UserForm {
    private String login;
    private String pwd;
}